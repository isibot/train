#include "mbed.h"
#include <array>
#include "Player.hpp"

void allumerLesFeux(DigitalOut & rouge, DigitalOut & orange, DigitalOut & vert)
{
    printf("Animation d'allumage des feux");
    rouge   = 1;
    orange  = 1;
    vert    = 1;

    thread_sleep_for(1000);
    rouge   = 0;
    thread_sleep_for(1000);
    orange  = 0;
    thread_sleep_for(1000);
    vert    = 0;
}

void eteindreLesFeux(DigitalOut & rouge, DigitalOut & orange, DigitalOut & vert)
{
    printf("Eteindre les feux");
    rouge   = 1;
    orange  = 1;
    vert    = 1;
}

template <typename P1, typename P2>
void attendreConfirmationDesJoueurs(P1 & p1, P2 & p2)
{
    printf("Attente de confirmation pour mettre les trains en position de départ");
    while (!(p1.isClicking() && p2.isClicking()));
}


template <typename P1, typename P2>
void remettreEnPositionDeDepart(P1 & p1, P2 & p2)
{
    printf("Remise en position de départ");
    p1.finish();
    p1.stop();
    printf("\t- P1 est prêt pour relancer\n");

    p2.finish();
    p2.stop();
    printf("\t- P2 est prêt pour relancer\n");
}

template <typename P1, typename P2>
void jouer(P1 & p1, P2 & p2)
{
    while (not (p1.hasFinished() || p2.hasFinished())) {
        p1.updateSpeed();
        p2.updateSpeed();

        p1.print();
        thread_sleep_for(1);
    }
}

int main()
{
    bool direction{};
    printf("Start\n");
    // Déclaration des entrées et sorties utilisées pour chaque élément
    Player<A0, D8, D9, A1, A2> felix{direction};
    Player<A4, D4, D5, D2, D3> jeremy{direction};

    DigitalOut feuRouge(D13);
    DigitalOut feuOrange(D12);
    DigitalOut feuVert(D11);
    DigitalOut driverEnable(D10);

    while(true)
    {
        driverEnable = 0;
        printf("Light\n");
        allumerLesFeux(feuRouge, feuOrange, feuVert);
        driverEnable = 1;
        printf("Play2\n");
        jouer(felix, jeremy);
        printf("Wait\n");
        felix.stop();
        jeremy.stop();
        attendreConfirmationDesJoueurs(felix, jeremy);
        printf("Reset\n");
        remettreEnPositionDeDepart(felix, jeremy);

        // Changement de sens pour la partie suivante.
        direction = !direction;
    }

    //system_reset();
    return 0;
}



