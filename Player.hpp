#pragma once

#include <algorithm>
#include <cstdio>

#include "mbed.h"

template <PinName pinBut, PinName pinMotA, PinName pinMotB, PinName start, PinName end>
class Player
{
    // Bouton de commande
    mbed::InterruptIn   _interrupt;
    // Controle du moteur
    DigitalOut          _motA;
    PwmOut              _motB;
    // Capteur de fin de circuit
    DigitalIn           _bout1;
    DigitalIn           _bout2;
    
    // Gestion de la vitesse
    unsigned            _tickCounter = 0;
    float               _speed = 0;
    // Gestion de la direction
    bool &              _direction;

    void click()
    {
        _speed += 100;
    }

public:
    Player(bool & direction):
        _interrupt(pinBut),
        _motA(pinMotA),
        _motB(pinMotB),
        _bout1(start),
        _bout2(end),
        _direction(direction)
    {
        _motA = 0;
        _motB = 0;

        _interrupt.rise(callback(this, &Player::click));
    }

    void updateSpeed()
    {
        _speed -= 0.01f * (_speed + 100.f);
        _speed = std::min(1000.f, _speed);
        _speed = std::max(0.f,    _speed);

        _motB = _speed;
    }

    void print()
    {
        printf("%d\n", static_cast<int>(_motB.read() * 100.f));
    }

    void finish()
    {
        _motA = static_cast<float>(!_direction);
        _motB = static_cast<float>(_direction);
        while((_direction && _bout2.read() != 0) || (!_direction && _bout1.read() != 0));
    }

    bool hasFinished()
    {
        return (_direction && _bout2.read() == 0) || (!_direction && _bout1.read() == 0);
    }

    bool isClicking()
    {
        return _interrupt.read();
    }

    void stop()
    {
        _motA = 0;
        _motB = 0;
    }
};
